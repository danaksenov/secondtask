package com.example.second1;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText login;
    EditText email;
    EditText phoneNumber;
    EditText password1;
    EditText password2;
    Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        login = findViewById(R.id.login);
        email = findViewById(R.id.email);
        phoneNumber = findViewById(R.id.phone);
        password1 = findViewById(R.id.password);
        password2 = findViewById(R.id.password_check);
        button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login.getText().toString().isEmpty() &&
                        email.getText().toString().isEmpty() &&
                        phoneNumber.toString().isEmpty() &&
                        password1.toString().isEmpty() &&
                        password2.toString().isEmpty()) {
                   AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                           .setTitle("Error")
                           .setMessage("Data invalid")
                           .setPositiveButton("Login", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   dialog.dismiss();
                               }
                           }).create();
                   dialog.show();
                } else {
                    Toast.makeText(v.getContext(),"Welcome", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}